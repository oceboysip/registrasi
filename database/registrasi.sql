-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 11, 2018 at 10:03 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.1.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `registrasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `reg_toko`
--

CREATE TABLE `reg_toko` (
  `reg_id` int(10) UNSIGNED NOT NULL,
  `reg_nama_toko` varchar(255) DEFAULT NULL,
  `reg_no_handphone` varchar(255) DEFAULT NULL,
  `reg_email` varchar(255) DEFAULT NULL,
  `reg_sosmed` varchar(255) DEFAULT NULL,
  `reg_domisili` varchar(255) DEFAULT NULL,
  `reg_jenis_usaha` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reg_toko`
--

INSERT INTO `reg_toko` (`reg_id`, `reg_nama_toko`, `reg_no_handphone`, `reg_email`, `reg_sosmed`, `reg_domisili`, `reg_jenis_usaha`) VALUES
(1, 'cek soundf', '09202029', 'imam.maulana2011@gmail.com', 'imam.maulana2011@gmail.com', 'jakarta', NULL),
(2, 'toko sabun', '08229292911', 'budi@gmail.com', 'budi@gmail.com', 'jakarta', NULL),
(3, 'toko batik', '23232323311', 'admin@catat.us', 'budi@gmail.com', 'ASDASD', NULL),
(4, 'cek soundf', '09202029', 'anas@shinhan.com', 'anas@shinhan.com', 'jakartA', NULL),
(5, 'toko batik pekalongan', '0929929292', 'imam@imamx.com', 'imam.maulana2011@gmail.com', 'jakarta', 'kang sayur'),
(6, 'toko minuman', '0821100738339', 'imam1@gmail.com', 'imam1@gmail.com', 'jakarta', 'dagang somay');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `reg_toko`
--
ALTER TABLE `reg_toko`
  ADD PRIMARY KEY (`reg_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `reg_toko`
--
ALTER TABLE `reg_toko`
  MODIFY `reg_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

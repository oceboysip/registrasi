<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//Untuk Print_r 
if ( ! function_exists('pr'))
{
	function pr($obj_name){
		print "<pre>";
		print_r($obj_name);
		print "</pre>";
	}
}
<?php
class lhbu_model extends CI_Model{
  function __construct() {
    parent::__construct();
  }


  public function record_count($req=array()) {
    if($req){
      $arraynya=array();
      if($req['NoRef']){
        $arraynya['NoTransaksi'] = $req['NoRef'];
        $this->db->where('NoTransaksi',$req['NoRef']);
      }
      if($req['JenisTransaksi']){
        if($req['JenisTransaksi'] != '.: Please Select :.'){ 
          $arraynya['jenis_rekening'] = $req['JenisTransaksi'];
          $this->db->where('jenis_rekening',$req['JenisTransaksi']);
        }
      }
      if($req['DariTanggal'] || $req['SampaiTanggal']){
      $arraypecahdari=explode('/',$req['DariTanggal']);
		$haridari=$arraypecahdari[0];
		$bulandari=$arraypecahdari[1];
		$tahundari=$arraypecahdari[2];
		
		$tanggaldari=$tahundari.'-'.$bulandari.'-'.$haridari;
		
		$arraypecahsampai=explode('/',$req['SampaiTanggal']);
		$harisampai=$arraypecahsampai[0];
		$bulansampai=$arraypecahsampai[1];
		$tahunsampai=$arraypecahsampai[2];
		$tanggalsampai=$tahunsampai.'-'.$bulansampai.'-'.$harisampai;
	 
	 
	  //pr($tanggaldari."   ".$tanggalsampai);exit;

      $this->db->where('Tanggal_Valuta >=', $tanggaldari );
      $this->db->where('Tanggal_Valuta <=', $tanggalsampai );
     }
     
     $query = $this->db->get('lhbu');

   }else{
   
     $query = $this->db->get("lhbu");

   }

   $rowcount = $query->num_rows();

   return $rowcount;
 }

 
public function fetch_lhbu_report($req=array()){
	$arraynya=array();
    if(@$req['NoRef']){
      $arraynya['NoTransaksi'] = $req['NoRef'];
      $this->db->where('NoTransaksi',$req['NoRef']);
    }
    if(@$req['JenisTransaksi']){
      if($req['JenisTransaksi'] != '.: Please Select :.'){ 
        $arraynya['jenis_rekening'] = $req['JenisTransaksi'];
        $this->db->where('jenis_rekening',$req['JenisTransaksi']);
      }
    }


    if(@$req['DariTanggal'] || @$req['SampaiTanggal']){
    $arraypecahdari=explode('/',$req['DariTanggal']);
		$haridari=$arraypecahdari[0];
		$bulandari=$arraypecahdari[1];
		$tahundari=$arraypecahdari[2];
		
		$tanggaldari=$tahundari.'-'.$bulandari.'-'.$haridari;
		
		$arraypecahsampai=explode('/',$req['SampaiTanggal']);
		$harisampai=$arraypecahsampai[0];
		$bulansampai=$arraypecahsampai[1];
		$tahunsampai=$arraypecahsampai[2];
		$tanggalsampai=$tahunsampai.'-'.$bulansampai.'-'.$harisampai;
	 
	 
	  //pr($tanggaldari."   ".$tanggalsampai);exit;

       $this->db->where('Tanggal_Valuta >=', $tanggaldari );
      $this->db->where('Tanggal_Valuta <=', $tanggalsampai );
    }

            //exit;
 $query = $this->db->get('lhbu');
            //pr($req);
            //pr($query);exit;
            //pr($arraynya);exit;
            //$query = $this->db->get_where('lld_main_table', $arraynya);

    if ($query->num_rows() > 0) {
      foreach ($query->result() as $row) {
        $data[] = $row;
      }
      return $data;
    }
	
}}


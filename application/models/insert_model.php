<?php
class insert_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }
   function form_insert($data){
		//pr("ss");exit;
        return  $this->db->insert('lld_main_table', $data);
    }

    public function record_count($req=array()) {
        if($req){
            $arraynya=array();
            if($req['NoRef']){
                $arraynya['NoTransaksi'] = $req['NoRef'];
                $this->db->where('NoTransaksi',$req['NoRef']);
            }
            if($req['JenisTransaksi']){
                if($req['JenisTransaksi'] != '.: Please Select :.'){ 
                    $arraynya['jenis_rekening'] = $req['JenisTransaksi'];
                    $this->db->where('jenis_rekening',$req['JenisTransaksi']);
                }
            }
            if($req['DariTanggal'] || $req['SampaiTanggal']){
             $this->db->where('Tanggal_Transaksi >=', date('Y-m-d', strtotime($req['DariTanggal'] )) );
             $this->db->where('Tanggal_Transaksi <=', date('Y-m-d', strtotime($req['SampaiTanggal'])) );
         }

         $query = $this->db->get('lld_main_table');

     }else{

       $query = $this->db->get("lld_main_table");

   }

   $rowcount = $query->num_rows();

   return $rowcount;
}




public function fetch_lld($limit, $start,$req=array()) {

    if($req){
        $this->db->limit($limit, $start);

        $arraynya=array();
        if($req['NoRef']){
            $arraynya['NoTransaksi'] = $req['NoRef'];
            $this->db->where('NoTransaksi',$req['NoRef']);
        }
        if($req['JenisTransaksi']){
            if($req['JenisTransaksi'] != '.: Please Select :.'){ 
                $arraynya['jenis_rekening'] = $req['JenisTransaksi'];
                $this->db->where('jenis_rekening',$req['JenisTransaksi']);
            }
        }


        if($req['DariTanggal'] || $req['SampaiTanggal']){

            $this->db->where('Tanggal_Transaksi >=', date('Y-m-d', strtotime($req['DariTanggal'] )) );
            $this->db->where('Tanggal_Transaksi <=', date('Y-m-d', strtotime($req['SampaiTanggal'])) );
        }

            //exit;
		
        $query = $this->db->get('lld_main_table');
            //pr($req);
            //pr($query);exit;

            //pr($arraynya);exit;
            //$query = $this->db->get_where('lld_main_table', $arraynya);

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }else{

        $this->db->limit($limit, $start);
        $query = $this->db->get("lld_main_table");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
               //W print_r($data);exit;
            return $data;

        }

    }
    return false;
}

public function fetch_lldtrans($req=array()) {

    if($req){


        $arraynya=array();
        $tahun=$req['tahun'];
        $bulan=$req['bulan'];

            //exit;

        $query = $this->db->query('select * from lld_main_table 
            where 1=1 AND status_posisi=1 AND MONTH(Tanggal_Transaksi) = '.$bulan.' AND YEAR(Tanggal_Transaksi) = '.$tahun.'
    ');
          


            //$query = $this->db->get_where('lld_main_table', $arraynya);

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }else{

     $arraynya=array();
     $tahun=$req['tahun'];
     $bulan=$req['bulan']-1;

            //exit;

     $query = $this->db->query('select * from lld_main_table 
        where 1=1 AND status_posisi=1 AND MONTH(Tanggal_Transaksi) = '.$bulan.' AND YEAR(Tanggal_Transaksi) = '.$tahun.'
        group by NegaraKD,JenisValas,jenis_rekening');
     if ($query->num_rows() > 0) {
        foreach ($query->result() as $row) {
            $data[] = $row;
        }
        return $data;
    }

}
return false;
}

}
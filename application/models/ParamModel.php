<?php
class ParamModel extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    function form_insert($data){
        // Inserting in Table(students) of Database(college)
        $this->db->insert('lld_main_table', $data);
    }


  
    public function fetch_KodeNegara() {
       
        $query = $this->db->get("lldcountries");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function fetch_Valas() {
       
        $query = $this->db->get("lldvalas");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }


public function jenis_identifikasi() {
    //pr('s');exit;
   
        $query = $this->db->get('jenis_identifikasi');
       
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
 }

    public function fetch_Category() {
       
        $query = $this->db->get("categories");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function tujuan_transaksis() {
       
        $query = $this->db->get("tujuan_transaksis");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function jenis_rekenings() {
       
        $query = $this->db->get("jenis_rekenings");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
   

   



}
<?php
class rte_model extends CI_Model{
  function __construct() {
    parent::__construct();
  }
  
	function get_rte_data($Id){

	  $query = $this->db->get_where("rte_main_table", array('id'=>$Id));

	  if ($query->num_rows() > 0) {
		foreach ($query->result() as $row) {
		  $data[] = $row;
		}
		return $data;
	  }
	  return false;

	}
	function update_rte($data){
        // Inserting in Table(students) of Database(college)
	 $this->db->where('id',$data['id'])->update('rte_main_table', $data);
	}
}
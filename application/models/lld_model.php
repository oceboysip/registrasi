<?php
class lld_model extends CI_Model{
  function __construct() {
    parent::__construct();
  }
  function form_insert($data){

    $this->db->insert('lld_main_table', $data);
  }


  function form_insert_rte($data){
    $this->db->insert('rte_main_table', $data);
  }

  public function record_count($req=array()) {
    if($req){
      $arraynya=array();
      if($req['NoRef']){
        $arraynya['NoTransaksi'] = $req['NoRef'];
        $this->db->where('NoTransaksi',$req['NoRef']);
      }
      if($req['JenisTransaksi']){
        if($req['JenisTransaksi'] != '.: Please Select :.'){ 
          $arraynya['jenis_rekening'] = $req['JenisTransaksi'];
          $this->db->where('jenis_rekening',$req['JenisTransaksi']);
        }
      }
      if($req['DariTanggal'] || $req['SampaiTanggal']){
      $arraypecahdari=explode('/',$req['DariTanggal']);
		$haridari=$arraypecahdari[0];
		$bulandari=$arraypecahdari[1];
		$tahundari=$arraypecahdari[2];
		
		$tanggaldari=$tahundari.'-'.$bulandari.'-'.$haridari;
		
		$arraypecahsampai=explode('/',$req['SampaiTanggal']);
		$harisampai=$arraypecahsampai[0];
		$bulansampai=$arraypecahsampai[1];
		$tahunsampai=$arraypecahsampai[2];
		$tanggalsampai=$tahunsampai.'-'.$bulansampai.'-'.$harisampai;
	 
	 
	  //pr($tanggaldari."   ".$tanggalsampai);exit;

      $this->db->where('Tanggal_Transaksi >=', $tanggaldari );
      $this->db->where('Tanggal_Transaksi <=', $tanggalsampai );
     }
     $this->db->where('status_posisi','1');
     $query = $this->db->get('lld_main_table');

   }else{
     $this->db->where('status_posisi','1');
     $query = $this->db->get("lld_main_table");

   }

   $rowcount = $query->num_rows();

   return $rowcount;
 }

 
public function fetch_lld_report($req=array()){
	$arraynya=array();
    if($req['NoRef']){
      $arraynya['NoTransaksi'] = $req['NoRef'];
      $this->db->where('NoTransaksi',$req['NoRef']);
    }
    if($req['JenisTransaksi']){
      if($req['JenisTransaksi'] != '.: Please Select :.'){ 
        $arraynya['jenis_rekening'] = $req['JenisTransaksi'];
        $this->db->where('jenis_rekening',$req['JenisTransaksi']);
      }
    }


    if($req['DariTanggal'] || $req['SampaiTanggal']){
    $arraypecahdari=explode('/',$req['DariTanggal']);
		$haridari=$arraypecahdari[0];
		$bulandari=$arraypecahdari[1];
		$tahundari=$arraypecahdari[2];
		
		$tanggaldari=$tahundari.'-'.$bulandari.'-'.$haridari;
		
		$arraypecahsampai=explode('/',$req['SampaiTanggal']);
		$harisampai=$arraypecahsampai[0];
		$bulansampai=$arraypecahsampai[1];
		$tahunsampai=$arraypecahsampai[2];
		$tanggalsampai=$tahunsampai.'-'.$bulansampai.'-'.$harisampai;
	 
	 
	  //pr($tanggaldari."   ".$tanggalsampai);exit;

      $this->db->where('Tanggal_Transaksi >=', $tanggaldari );
      $this->db->where('Tanggal_Transaksi <=', $tanggalsampai );
    }

            //exit;
    $this->db->where('status_posisi','1');
    $query = $this->db->get('lld_main_table');
            //pr($req);
            //pr($query);exit;
            //pr($arraynya);exit;
            //$query = $this->db->get_where('lld_main_table', $arraynya);

    if ($query->num_rows() > 0) {
      foreach ($query->result() as $row) {
        $data[] = $row;
      }
      return $data;
    }
	
}
 public function record_count_supervisi($req=array()) {
  if($req){
    $arraynya=array();
    if($req['NoRef']){
      $arraynya['NoTransaksi'] = $req['NoRef'];
      $this->db->where('NoTransaksi',$req['NoRef']);
    }
    if($req['JenisTransaksi']){
      if($req['JenisTransaksi'] != '.: Please Select :.'){ 
        $arraynya['jenis_rekening'] = $req['JenisTransaksi'];
        $this->db->where('jenis_rekening',$req['JenisTransaksi']);
      }
    }
    if($req['DariTanggal'] || $req['SampaiTanggal']){
     $this->db->where('Tanggal_Transaksi >=', date('Y-m-d', strtotime($req['DariTanggal'] )) );
     $this->db->where('Tanggal_Transaksi <=', date('Y-m-d', strtotime($req['SampaiTanggal'])) );
   }
   $this->db->where('status_posisi','0');
   $query = $this->db->get('lld_main_table');

 }else{
   $this->db->where('status_posisi','0');
   $query = $this->db->get("lld_main_table");

 }

 $rowcount = $query->num_rows();

 return $rowcount;
}

public function record_count_rte($req=array()) {
  if($req){
    $arraynya=array();
    if($req['NoRef']){
      $arraynya['NoTransaksi'] = $req['NoRef'];
      $this->db->where('NoTransaksi',$req['NoRef']);
    }
    if($req['JenisTransaksi']){
      if($req['JenisTransaksi'] != '.: Please Select :.'){ 
        $arraynya['jenis_rekening'] = $req['JenisTransaksi'];
        $this->db->where('jenis_rekening',$req['JenisTransaksi']);
      }
    }
       if($req['DariTanggal']){
		//$arraypecahdari=explode('/',$req['DariTanggal']);
		$this->db->where('TanggalPeriode =', $req['DariTanggal'] );
		}
   $query = $this->db->get('rte_main_table');

 }else{

   $query = $this->db->get("rte_main_table");

 }

 $rowcount = $query->num_rows();

 return $rowcount;
}

public function fetch_lld_maintotal($req=array()){
	$tahunsekarang=$req['tahun'];
	$tahun=$req['tahun'];
	$bulan=$req['bulan'];
  
	if($req['bulan']=='01'){
	    $req['tahun']=$req['tahun']-1;
		$req['bulan']='13';
		$tahunsekarang=$req['tahun'];
		$tahun=$req['tahun'];
		$bulan=$req['bulan'];
	}
	if($req['bulan']=='02'){
	    $req['tahun']=$req['tahun']-1;
		$tahunsekarang=$req['tahun']+1;
		$tahun=$req['tahun'];
		$bulan=$req['bulan'];
	}
  

  $BulanPosisi=(int)$bulan-1;
  
  //pr($tahunsekarang.$tahun.$bulan.$BulanPosisi);exit;
  
   if($BulanPosisi < 10){
	  $BulanPosisi='0'.$BulanPosisi;
  }
  $BulanPosisisebelum=(int)$bulan-2;
  if($BulanPosisisebelum=="00"){
	  $BulanPosisisebelum='12';
  }
   if($BulanPosisisebelum < 10){
	  $BulanPosisisebelum='0'.$BulanPosisisebelum;
  }
  //pr($BulanPosisi);exit;
  $TotalSeharusnya="0";
  $noparamposisi="0";
  $Debet="0";
  $Kredit="0";
  $totalXX="0";
  $TotalPosisiSebelumnya="0";
  $StringResult="";
  $presisi="+";
  $status=" AND status_posisi=1";
  $SelectMainTable=$this->db->query('select * from lld_main_table 
    where 1=1 AND MONTH(Tanggal_Transaksi) = '.$BulanPosisi.' AND YEAR(Tanggal_Transaksi) = '.$tahunsekarang.' '.$status.'
    group by NegaraKD,JenisValas,jenis_rekening');
	 //pr('ss');exit;
$noparamposisisendiri=0;
  //untuk kondisi totalposisi yang sendirian
$FecthPosisi2=$this->db->query("select * from totalposisi where BulanPosisi='".$BulanPosisisebelum."'");
//pr("select * from totalposisi where BulanPosisi='".$BulanPosisisebelum."'");exit;
//pr("select * from totalposisi where BulanPosisi='".$BulanPosisisebelum."'");exit;
  if ($FecthPosisi2->num_rows() > 0) {
    
    foreach ($FecthPosisi2->result() as $row2) {
     $negarakode=substr($row2->MataUang,0,2);
	// pr($negarakode);exit;
	
      $paramdk=$this->db->query("select sum(jumtransaksi) as total,JenisTransaksi from lld_main_table  
        where 1=1 AND MONTH(Tanggal_Transaksi) = '".$BulanPosisi."' AND YEAR(Tanggal_Transaksi) = '".$tahunsekarang."' ".$status."
        and jenis_rekening='".$row2->JenisRekening."' and NegaraKD='".$row2->KodeNegara."' and JenisValas='".$row2->MataUang."'
        group by NegaraKD,JenisValas,jenis_rekening");

      if ($paramdk->num_rows() == 0) {
		 
         $noparamposisisendiri++;
         $StringResult=$StringResult.'152000'.$tahunsekarang.''.$BulanPosisi.str_pad(TRIM($row2->JenisRekening), 2).str_pad($row2->KodeNegara, 2).str_pad($row2->MataUang, 3);
         $TotalPosisiSebelumnya=$row2->TotalPosisi;
          if($TotalPosisiSebelumnya < 0){$presisi="-";}else{$presisi="+";}
         $StringResult=$StringResult.str_pad(TRIM($TotalPosisiSebelumnya),18,"0",STR_PAD_LEFT).str_pad(TRIM("0"),18,"0",STR_PAD_LEFT).str_pad(TRIM("0"),18,"0",STR_PAD_LEFT).$presisi.str_pad("0",18,"0",STR_PAD_LEFT).str_pad(number_format($TotalPosisiSebelumnya,0,'',''),18,"0",STR_PAD_LEFT).PHP_EOL;
        $this->form_insert_posisi($tahunsekarang,$bulan,$row2->JenisRekening,$row2->KodeNegara,$row2->MataUang,$TotalPosisiSebelumnya);
     }
   }
 }
 //pr($StringResult);exit;


//untuk kondisi totalposisi yang udah pada join
  if ($SelectMainTable->num_rows() > 0) {
	
      $noparamposisi=0;
	 // pr($SelectMainTable->result());exit;
    foreach ($SelectMainTable->result() as $row) {
      $noparamposisi++;
      $jenis_rekening=TRIM($row->jenis_rekening);
      $NegaraKD=TRIM($row->NegaraKD);
      $JenisValas=TRIM($row->JenisValas);


			
      $FecthPosisi=$this->db->query("select * from totalposisi where TahunPosisi='".$tahun."' AND BulanPosisi = '".$BulanPosisisebelum."' AND
        JenisRekening='".$row->jenis_rekening."'  AND
        KodeNegara='".$row->NegaraKD."'  AND 
        MataUang='".$row->JenisValas."' ");
			
  
      if($FecthPosisi->num_rows()>'0'){
        $TotalPosisiSebelumnya=$FecthPosisi->result()[0]->TotalPosisi;
      }else{
        $TotalPosisiSebelumnya=0;
      }
		
      $StringResult=$StringResult.'152000'.$tahunsekarang.''.$BulanPosisi.str_pad(TRIM($row->jenis_rekening), 2).str_pad($row->NegaraKD, 2).str_pad($row->JenisValas, 3);
	
        $SelectMainTable2=$this->db->query("select * from lld_main_table 
        where 1=1 AND MONTH(Tanggal_Transaksi) = '".$BulanPosisi."' AND YEAR(Tanggal_Transaksi) = '".$tahunsekarang."' ".$status."
        and jenis_rekening='".$row->jenis_rekening."' and NegaraKD='".$NegaraKD."' and JenisValas='".$JenisValas."'
        group by NegaraKD,JenisValas,jenis_rekening");
		
	

        if ($SelectMainTable2->num_rows() > 0) {
          foreach ($SelectMainTable2->result() as $rowmain) {
	//pr($SelectMainTable2->result());exit;
            if($rowmain->jenis_rekening!='4A'){
			$Debet=0;
                  $Kredit=0;
		
             
                $paramdk=$this->db->query("select sum(jumtransaksi) as total,JenisTransaksi from lld_main_table  
                  where 1=1 AND MONTH(Tanggal_Transaksi) = '".$BulanPosisi."' AND YEAR(Tanggal_Transaksi) = '".$tahunsekarang."' ".$status."
                  and jenis_rekening='".$jenis_rekening."' and NegaraKD='".$NegaraKD."' and JenisValas='".$JenisValas."'
                  group by NegaraKD,JenisValas,jenis_rekening,JenisTransaksi");
		
			
                if ($paramdk->num_rows() > 0) {

                  foreach ($paramdk->result() as $rowparamdk) {
                    if($rowparamdk->JenisTransaksi=='1'){
                      $Debet=$rowparamdk->total*100;
                    }else{ 

                      $Kredit=$rowparamdk->total*100;
                    }

                  }
                }else{
					
                  $Debet=0*100;
                  $Kredit=0*100;
                }
					//pr($Kredit);exit;
			
              

              $totalakhir=$TotalPosisiSebelumnya+$Debet-$Kredit;
			  //pr($totalakhir);exit;
              if($totalakhir < 0){$presisi="-";}else{$presisi="+";}

              $StringResult=$StringResult.str_pad(TRIM($TotalPosisiSebelumnya),18,"0",STR_PAD_LEFT).str_pad(TRIM($Debet),18,"0",STR_PAD_LEFT).str_pad(TRIM($Kredit),18,"0",STR_PAD_LEFT).$presisi.str_pad("0",18,"0",STR_PAD_LEFT).str_pad(number_format($totalakhir,0,'',''),18,"0",STR_PAD_LEFT).PHP_EOL;
            
				
		
			  $this->form_insert_posisi($tahunsekarang,$bulan,$jenis_rekening,$NegaraKD,$JenisValas,$totalakhir);
				
            }else{
				
				 
                           // pr($row->id);exit; 
              $FecthPosisi2=$this->db->query("select * from totalposisi where TahunPosisi='".$tahun."' AND BulanPosisi='".$BulanPosisisebelum."' AND JenisRekening='".$rowmain->jenis_rekening."' AND KodeNegara='".$rowmain->NegaraKD."' AND MataUang='".$rowmain->JenisValas."'");
              
				//pr("select * from totalposisi where TahunPosisi='".$tahun."' AND BulanPosisi='0".$BulanPosisisebelum."' AND JenisRekening='".$rowmain->jenis_rekening."' AND KodeNegara='".$rowmain->NegaraKD."' AND MataUang='".$rowmain->JenisValas."'");exit;
			//pr($FecthPosisi2->result());exit;
              if ($FecthPosisi2->num_rows() > 0) {
              
                foreach ($FecthPosisi2->result() as $row2) {
           
                  $paramdk=$this->db->query("select sum(jumtransaksi) as total,JenisTransaksi from lld_main_table  
                    where 1=1 AND MONTH(Tanggal_Transaksi) = '".$BulanPosisi."' AND YEAR(Tanggal_Transaksi) = '".$tahunsekarang."' ".$status."
                    and jenis_rekening='".$row2->JenisRekening."' and NegaraKD='".$row2->KodeNegara."' and JenisValas='".$row2->MataUang."'
                    group by NegaraKD,JenisValas,jenis_rekening,JenisTransaksi");
				
                  if ($paramdk->num_rows() > 0) {
                    foreach ($paramdk->result() as $rowparamdk) {
                      if($rowparamdk->JenisTransaksi=='1'){
                        $Kredit=$rowparamdk->total*100;
                      }else{ 
                        $Debet=$rowparamdk->total*100;
                      }
                    }
                  }else{
				
					 $Debet=0*100;
					 $Kredit=0*100;
				  }
				  //pr($Kredit);exit
                  $totalakhir=$TotalPosisiSebelumnya-$Debet+$Kredit;
                   if($totalakhir < 0){$presisi="-";}else{$presisi="+";}
                  $StringResult=$StringResult.str_pad(TRIM($TotalPosisiSebelumnya),18,"0",STR_PAD_LEFT).str_pad(TRIM($Debet),18,"0",STR_PAD_LEFT).str_pad(TRIM($Kredit),18,"0",STR_PAD_LEFT).$presisi.str_pad("0",18,"0",STR_PAD_LEFT).str_pad(number_format($totalakhir,0,'',''),18,"0",STR_PAD_LEFT).PHP_EOL;
                //pr($StringResult);exit;
				 $this->form_insert_posisi($tahunsekarang,$bulan,$row2->JenisRekening,$row2->KodeNegara,$row2->MataUang,$totalakhir);
                }
				
				//pr($StringResult);exit;
              }
			  
            }
          }
        }

      }
  }




   //  pr($StringResult);exit;
 
 $totalrow=(int)$noparamposisisendiri+(int)$noparamposisi;
 $Stringheaderfooter='152000LLD2'.$tahunsekarang.$bulan.str_pad($totalrow,8,"0",STR_PAD_LEFT).PHP_EOL;
 $stringgabung=$Stringheaderfooter.$StringResult.$Stringheaderfooter;



return $stringgabung;











}

 function form_insert_posisi($tahun,$bulan,$JenisRekening,$KodeNegara,$MataUang,$Nominal){
  //pr("masuk");exit;
  $posisibulan=(int)$bulan-1;
  if($posisibulan < 10){
	  $posisibulan='0'.$posisibulan;
  }
  $data=array( 'TahunPosisi' =>$tahun,
            'BulanPosisi' =>$posisibulan,
            'JenisRekening' => $JenisRekening,
            'KodeNegara' =>$KodeNegara,
            'MataUang' =>$MataUang,
            'TotalPosisi' =>$Nominal
            );

        $query = $this->db->query("SELECT * FROM totalposisi
         WHERE TahunPosisi = '".$tahun."' 
         AND BulanPosisi='".$posisibulan."'
         AND JenisRekening='".$JenisRekening."'
         AND KodeNegara='".$KodeNegara."'
         AND MataUang='".$MataUang."'
                  limit 1");
//pr($posisibulan);exit;
        if($query->num_rows() == 0){
			//pr('s');exit;
            $this->db->insert('totalposisi', $data);
        }else{
            $this->db->where($data);
			//pr($posisibulan);exit;
            $this->db->update('totalposisi', $data);
        }

        return false;
  }




public function fetch_lld_rte($req=array()){

  $StringResult="";

  $bulan=$req['bulan'];
  $tahun=$req['tahun'];
  $tahuns=$req['tahun']+1;
  
  $bulanheader = $bulan+1;
		if($bulanheader<10){
		$bulanheader  = '0'.$bulanheader ;
		}
		
  $SelectMainRTE=$this->db->query("select * from rte_main_table 
    where 1=1 AND TanggalPeriode ='".$bulan."-".$tahun."'");

     //pr( $SelectMainRTE);exit;
	if($req['bulan']==12){
	$StringResult=$StringResult.'152000LLD3'.$tahuns.'01'.str_pad($SelectMainRTE->num_rows(),8,"0",STR_PAD_LEFT).PHP_EOL;
	}else{
	$StringResult=$StringResult.'152000LLD3'.$tahun.$bulanheader.str_pad($SelectMainRTE->num_rows(),8,"0",STR_PAD_LEFT).PHP_EOL;
	}
	if ($SelectMainRTE->num_rows() > 0) {
	   $jumlahdesimal='2';
				$pemisahdesimal=',';
				$pemisahribuan='';
				
    foreach ($SelectMainRTE->result() as $row) {
      $StringResult=$StringResult.'152000'.$tahun.$bulan.str_pad($row->no_identifikasi,16," ",STR_PAD_RIGHT);
	  $explodetgl=explode("/",str_pad( $row->tanggal_peb, 8));
	  //pr(str_pad($row->sandi_ket, 4).str_pad($row->Dokumen, 1).str_pad($row->jenis_va_peb, 3));
	  //pr(str_pad($row->jumlah_peb,18,"0",STR_PAD_LEFT).str_pad($row->sandi_ket, 4).str_pad($row->Dokumen, 1).str_pad($row->jenis_va_peb, 3));exit;
      $StringResult=$StringResult.str_pad(str_replace(str_split('\\/:*?"<>-.|'), "", $row->npwp),15,"0",STR_PAD_LEFT).str_pad($row->nama_penerima, 100).str_pad($row->sandi_pabean, 6).str_pad($row->no_daftar, 8);
	  
	  $jumlahdhe=number_format($row->jumlah_dhe,$jumlahdesimal,$pemisahdesimal,$pemisahribuan); 
	 
	  $jumlahdhefix=str_pad(str_replace(",","",$jumlahdhe),18,"0",STR_PAD_LEFT);
	  // pr($jumlahdhefix);exit;
	    $jumlahpeb=number_format($row->jumlah_peb,$jumlahdesimal,$pemisahdesimal,$pemisahribuan); 
	  $jumlahpebfix=str_pad(str_replace(",","",$jumlahpeb),18,"0",STR_PAD_LEFT);
	  
      $StringResult=$StringResult.$explodetgl[2].$explodetgl[1].$explodetgl[0].str_pad($row->jenis_valas, 3).$jumlahdhefix.$jumlahpebfix.str_pad($row->sandi_ket, 4).str_pad($row->Dokumen, 1).str_pad($row->jenis_va_peb, 3).PHP_EOL;
	 // pr($StringResult);exit;
	}
  }

  if($req['bulan']==12){
	$StringResult=$StringResult.'152000LLD3'.$tahuns.'01'.str_pad($SelectMainRTE->num_rows(),8,"0",STR_PAD_LEFT).PHP_EOL;
	}else{
	$StringResult=$StringResult.'152000LLD3'.$tahun.$bulanheader.str_pad($SelectMainRTE->num_rows(),8,"0",STR_PAD_LEFT).PHP_EOL;
	}
	//pr($StringResult);exit;
  return $StringResult;

}



public function fetch_lld($limit, $start,$req=array()) {

  if($req){
    $this->db->limit($limit, $start);

    $arraynya=array();
    if($req['NoRef']){
      $arraynya['NoTransaksi'] = $req['NoRef'];
      $this->db->where('NoTransaksi',$req['NoRef']);
    }
    if($req['JenisTransaksi']){
      if($req['JenisTransaksi'] != '.: Please Select :.'){ 
        $arraynya['jenis_rekening'] = $req['JenisTransaksi'];
        $this->db->where('jenis_rekening',$req['JenisTransaksi']);
      }
    }
	
	if($req['NegaraKD']){
      $arraynya['NegaraKD'] = $req['NegaraKD'];
      $this->db->where('NegaraKD',$req['NegaraKD']);
    }


    if($req['DariTanggal'] || $req['SampaiTanggal']){
		
		$arraypecahdari=explode('/',$req['DariTanggal']);
		$haridari=$arraypecahdari[0];
		$bulandari=$arraypecahdari[1];
		$tahundari=$arraypecahdari[2];
		
		$tanggaldari=$tahundari.'-'.$bulandari.'-'.$haridari;
		
		$arraypecahsampai=explode('/',$req['SampaiTanggal']);
		$harisampai=$arraypecahsampai[0];
		$bulansampai=$arraypecahsampai[1];
		$tahunsampai=$arraypecahsampai[2];
		$tanggalsampai=$tahunsampai.'-'.$bulansampai.'-'.$harisampai;
	 
	 
	  //pr($tanggaldari."   ".$tanggalsampai);exit;

      $this->db->where('Tanggal_Transaksi >=', $tanggaldari );
      $this->db->where('Tanggal_Transaksi <=', $tanggalsampai );
    }

            //exit;
   // $this->db->where('status_posisi','1');
    $query = $this->db->get('lld_main_table');
               //pr($query);exit;
            //pr($arraynya);exit;
            //$query = $this->db->get_where('lld_main_table', $arraynya);

    if ($query->num_rows() > 0) {
      foreach ($query->result() as $row) {
        $data[] = $row;
      }
      return $data;
    }
  }else{
    $this->db->where('status_posisi','1');
    $this->db->limit($limit, $start);
    $query = $this->db->get("lld_main_table");

    if ($query->num_rows() > 0) {
      foreach ($query->result() as $row) {
        $data[] = $row;
      }
               //W print_r($data);exit;
      return $data;

    }

  }
  return false;
}


public function fetch_lld_supervisi($limit, $start,$req=array()) {

  if($req){
    $this->db->limit($limit, $start);

    $arraynya=array();
    if($req['NoRef']){
      $arraynya['NoTransaksi'] = $req['NoRef'];
      $this->db->where('NoTransaksi',$req['NoRef']);
    }
    if($req['JenisTransaksi']){
      if($req['JenisTransaksi'] != '.: Please Select :.'){ 
        $arraynya['jenis_rekening'] = $req['JenisTransaksi'];
        $this->db->where('jenis_rekening',$req['JenisTransaksi']);
      }
    }


    if($req['DariTanggal'] || $req['SampaiTanggal']){

      $this->db->where('Tanggal_Transaksi >=', date('Y-m-d', strtotime($req['DariTanggal'] )) );
      $this->db->where('Tanggal_Transaksi <=', date('Y-m-d', strtotime($req['SampaiTanggal'])) );
    }

            //exit;
    $this->db->where('status_posisi','0');
    $query = $this->db->get('lld_main_table');
            //pr($req);
            //pr($query);exit;
            //pr($arraynya);exit;
            //$query = $this->db->get_where('lld_main_table', $arraynya);

    if ($query->num_rows() > 0) {
      foreach ($query->result() as $row) {
        $data[] = $row;
      }
      return $data;
    }
  }else{
    $this->db->where('status_posisi','0');
    $this->db->limit($limit, $start);
    $query = $this->db->get("lld_main_table");

    if ($query->num_rows() > 0) {
      foreach ($query->result() as $row) {
        $data[] = $row;
      }
               //W print_r($data);exit;
      return $data;

    }

  }
  return false;
}


public function  fetch_rte($limit, $start,$req=array()) {

  if($req){
	  
    $this->db->limit($limit, $start);

    $arraynya=array();
    if($req['NoRef']){
      $arraynya['NoTransaksi'] = $req['NoRef'];
      $this->db->where('NoTransaksi',$req['NoRef']);
    }
    if($req['JenisTransaksi']){
      if($req['JenisTransaksi'] != '.: Please Select :.'){ 
        $arraynya['jenis_rekening'] = $req['JenisTransaksi'];
        $this->db->where('jenis_rekening',$req['JenisTransaksi']);
      }
    }


    if($req['DariTanggal']){
		//$arraypecahdari=explode('/',$req['DariTanggal']);
		$this->db->where('TanggalPeriode =', $req['DariTanggal'] );
	}

            //exit;

    $query = $this->db->get('rte_main_table');
            //pr($req);
            //pr($query);exit;

            //pr($arraynya);exit;
            //$query = $this->db->get_where('lld_main_table', $arraynya);

    if ($query->num_rows() > 0) {
      foreach ($query->result() as $row) {
        $data[] = $row;
      }
      return $data;
    }
  }else{

    $this->db->limit($limit, $start);
    $query = $this->db->get("rte_main_table");

    if ($query->num_rows() > 0) {
      foreach ($query->result() as $row) {
        $data[] = $row;
      }
               //W print_r($data);exit;
      return $data;

    }

  }
  return false;
}

function get_lld_data($Id){

  $query = $this->db->get_where("lld_main_table", array('id'=>$Id));

  if ($query->num_rows() > 0) {
    foreach ($query->result() as $row) {
      $data[] = $row;
    }
    return $data;
  }
  return false;

}

function updatelld($data){
        // Inserting in Table(students) of Database(college)
 $this->db->where('id',$data['id'])->update('lld_main_table', $data);
}





public function fetch_lldtrans($req=array()) {

  if($req){


    $arraynya=array();
    if($req['NoRef']){
      $arraynya['NoTransaksi'] = $req['NoRef'];
      $this->db->where('NoTransaksi',$req['NoRef']);
    }
    if($req['JenisTransaksi']){
      if($req['JenisTransaksi'] != '.: Please Select :.'){ 
        $arraynya['jenis_rekening'] = $req['JenisTransaksi'];
        $this->db->where('jenis_rekening',$req['JenisTransaksi']);
      }
    }


    if($req['DariTanggal'] || $req['SampaiTanggal']){

      $this->db->where('Tanggal_Transaksi >=', date('Y-m-d', strtotime($req['DariTanggal'] )) );
      $this->db->where('Tanggal_Transaksi <=', date('Y-m-d', strtotime($req['SampaiTanggal'])) );
    }

            //exit;

    $query = $this->db->get('lld_main_table');
            //pr($req);
            //pr($query);exit;

            //pr($arraynya);exit;
            //$query = $this->db->get_where('lld_main_table', $arraynya);

    if ($query->num_rows() > 0) {
      foreach ($query->result() as $row) {
        $data[] = $row;
      }
      return $data;
    }
  }else{

    $this->db->limit($limit, $start);
    $query = $this->db->get("lld_main_table");

    if ($query->num_rows() > 0) {
      foreach ($query->result() as $row) {
        $data[] = $row;
      }
               //W print_r($data);exit;
      return $data;

    }

  }
  return false;
}

}
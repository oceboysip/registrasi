<?php
class reg extends CI_Controller {
    function __construct() {
        parent::__construct();
        session_start();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model(array('m_user'));
        if ($this->session->userdata('u_name')) {
            redirect('dashboard');
        }
    }
    function index() {

        $data['parsingout']="";
        $data['csrf']=array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );
        $this->load->view('registrasi',$data);
    }
    function proses() {

        if(isset($_POST['g-recaptcha-response']))
        {
            
            $secret_key="6Ldxrn8UAAAAAPwqmiuDUC9BPDV7628pzpI1R0Q-";
            $api_url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . $secret_key . '&response='.$_POST['g-recaptcha-response'];
            $response = @file_get_contents($api_url);
            $data = json_decode($response, true);
        
            if($data['success'])
            {
                    $nama_toko="";
                    $no_handphone ="";
                    $email="";
                    $sosmed="";
                    $domisili="";

                    $this->form_validation->set_rules('nama_toko', 'nama_toko', 'required|trim|xss_clean');
                    $this->form_validation->set_rules('no_handphone', 'no_handphone', 'required|trim|xss_clean');
                    $this->form_validation->set_rules('email', 'nama_toko', 'required|trim|xss_clean');
                    $this->form_validation->set_rules('sosmed', 'nama_toko', 'required|trim|xss_clean');
                    $this->form_validation->set_rules('domisili', 'nama_toko', 'required|trim|xss_clean');
                    $this->form_validation->set_rules('jenis_usaha', 'jenis_usaha', 'required|trim|xss_clean');
                     $this->form_validation->set_rules('jenis_sosmed', 'jenis_sosmed', 'required|trim|xss_clean');

                    if ($this->form_validation->run() == FALSE) {
                        //kalo validasi salah 
                        $this->session->set_flashdata('msg','Mohon koreksi data dengan Benar');
                        redirect('/');
                    } else {

                        $nama_toko = $this->input->post('nama_toko');
                        $no_handphone = $this->input->post('no_handphone');
                        $email = $this->input->post('email');
                        $sosmed = $this->input->post('sosmed');
                        $domisili = $this->input->post('domisili');
                        $jenis_usaha = $this->input->post('jenis_usaha');
                        $jenis_sosmed = $this->input->post('jenis_sosmed');

                        $this->db->where('reg_email',$email);
                        $q = $this->db->get('reg_toko');

                       if ( $q->num_rows() > 0 ) 
                       {
                          $this->session->set_flashdata('msg','Email Sudah Terdaftar , Mohon Gunakan Email lain !!!');
                          redirect('/');

                        } else {
                          $data_posting[] = array(
                                            'reg_nama_toko' => $nama_toko,
                                            'reg_no_handphone' => $no_handphone ,
                                            'reg_email' => $email,
                                            'reg_sosmed' => $sosmed,
                                            'reg_domisili' => $domisili,
                                            'reg_jenis_usaha' => $jenis_usaha,
                                            'reg_jenis_sosmed' => $jenis_sosmed
                                            );
                          //pr($data_posting);exit;
                          $this->db->insert('reg_toko',$data_posting[0]);
                          $this->session->set_flashdata('msg','Email Sukses Terdaftar !');
                          redirect('/');

                        }

                    }
            }
            else
            {   //Gagal Captcha google
                $success = false;
                $this->session->set_flashdata('msg','Mohon verifikasi data inputan ');
                redirect('/');
            }
        }
        else
        {
            //Kalau ngga ada Captcha 
            $success = false;
            $this->session->set_flashdata('msg','Mohon verifikasi data inputan ');
            redirect('/');
        }

    }
    function logout() {
        $this->session->sess_destroy();
        redirect('login');
    }
}
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="<?php echo base_url('assets/favicon.ico');?>" type="image/x-icon" />
  
  <title>Registrasi</title>

  <!-- Bootstrap -->
  <link href="<?php echo base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="<?php echo base_url('assets/vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
  <!-- NProgress -->
  <!-- Custom Theme Style -->
  <link href="<?php echo base_url('assets/build/css/custom.min.css'); ?>" rel="stylesheet">
  <!-- Dropzone.js -->
  <link href="<?php echo base_url('assets/vendors/dropzone/dist/min/dropzone.min.css'); ?>" rel="stylesheet">
</head>

      <body class="login">
       <div class="right_col" role="main">
        <div class="">
          <div class="page-title">
            <div class="clearfix"></div>
            <div class="row">

             <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Registrasi Gosend </small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Settings 1</a>
                        </li>
                        <li><a href="#">Settings 2</a>
                        </li>
                      </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <p class="text-muted font-13 m-b-30">

                  </p>
                  <div class="confirm-div">
                    <div class="callout callout-success">
                      <?php echo $this->session->flashdata('msg'); ?>
                    </div>
                  </div>
                  <br>
                  <div class="form-group">
                    <form action="<?php echo base_url('reg/proses');?>" method="POST" enctype="multipart/form-data">

                      <br>
                      <p>Nama Toko</p>
                      <input type="text" name="nama_toko" class="form form-control">
                      <br />
                       <br>
                      <p>Nomor Handphone</p>
                      <input type="text" name="no_handphone" class="form form-control">
                      <br />
                       <br>
                      <p>Email</p>
                      <input type="text" name="email" class="form form-control">
                      <br />
                       <br>
                      <p>Sosmed</p>
                       <select name="jenis_sosmed" class="form form-control col-md-6">
                        <option>Please Select</option>
                        <option>Twitter</option>
                        <option>Facebook</option>
                        <option>Instagram</option>
                       </select>

                       <input type="text" name="sosmed" class="form form-control">
                      <br />
                       <br>
                      <p>Domisili</p>
                      <input type="text" name="domisili" class="form form-control">
                      <br />
                       <br>
                      <p>Jenis Usaha</p>
                      <input type="text" name="jenis_usaha" class="form form-control">
                      <br />

                      <div class="form-group">
                      <div class="g-recaptcha" data-sitekey="6Ldxrn8UAAAAAE1PyLt2vTklMJQTcN62xkwTtRyJ"></div>
                      </div>
                      <br />
                      <input type="hidden" id="csrf_inp" name="<?php echo $csrf['name']; ?>" value ="<?php echo $csrf['hash'];?>" >
                      <button type="submit" class="btn btn-success btn-block">Submit</button>
                      <br />
                    </form>
                  </div>
                </div>

              </div>
            </div>
          </div> 
        </div>
      </div> 
      </div>

      </body>

    <script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js'); ?>"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url('assets/vendors/fastclick/lib/fastclick.js'); ?>"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url('assets/vendors/nprogress/nprogress.js'); ?>"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url('assets/vendors/dropzone/dist/min/dropzone.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendors/iCheck/icheck.min.js'); ?>"></script>
    <!-- Datatables -->
   
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url('assets/build/js/custom.min.js');?>"></script>
       <script src='https://www.google.com/recaptcha/api.js'></script>
</html>

-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.25-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for lhbu
CREATE DATABASE IF NOT EXISTS `lhbu` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `lhbu`;

-- Dumping structure for table lhbu.lhbu
CREATE TABLE IF NOT EXISTS `lhbu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_operasional` varchar(50) NOT NULL DEFAULT '0',
  `no_referensi` varchar(50) NOT NULL DEFAULT '0',
  `PUAB_DN_LN_DoC` varchar(50) NOT NULL DEFAULT '0',
  `SandiBank_Pemberi` varchar(50) NOT NULL DEFAULT '0',
  `Sandi_Negara_Pemberi` varchar(50) NOT NULL DEFAULT '0',
  `Sandi_Bank_Peminjam` varchar(50) NOT NULL DEFAULT '0',
  `Sandi_Negara_Peminjam` varchar(50) NOT NULL DEFAULT '0',
  `Mata_Uang` varchar(50) NOT NULL DEFAULT '0',
  `Volume_juta_rupiah` varchar(50) NOT NULL DEFAULT '0',
  `Volume_valuta_dasar` varchar(50) NOT NULL DEFAULT '0',
  `Suku_Bunga` varchar(50) NOT NULL DEFAULT '0',
  `Tanggal_Valuta` varchar(50) NOT NULL DEFAULT '0',
  `Tanggal_Jatuh_Tempo` varchar(50) NOT NULL DEFAULT '0',
  `Jangka_Waktu` varchar(50) NOT NULL DEFAULT '0',
  `Jam_Transaksi` varchar(50) NOT NULL DEFAULT '0',
  `Nama_Bank_Counterpart_PUAB_LN` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table lhbu.lhbu: ~0 rows (approximately)
/*!40000 ALTER TABLE `lhbu` DISABLE KEYS */;
INSERT INTO `lhbu` (`id`, `id_operasional`, `no_referensi`, `PUAB_DN_LN_DoC`, `SandiBank_Pemberi`, `Sandi_Negara_Pemberi`, `Sandi_Bank_Peminjam`, `Sandi_Negara_Peminjam`, `Mata_Uang`, `Volume_juta_rupiah`, `Volume_valuta_dasar`, `Suku_Bunga`, `Tanggal_Valuta`, `Tanggal_Jatuh_Tempo`, `Jangka_Waktu`, `Jam_Transaksi`, `Nama_Bank_Counterpart_PUAB_LN`) VALUES
	(1, '20', '500', 'avv', 'dela', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
	(2, '30', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
/*!40000 ALTER TABLE `lhbu` ENABLE KEYS */;

-- Dumping structure for table lhbu.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `NamaLengkap` varchar(100) DEFAULT NULL,
  `Position` varchar(100) DEFAULT NULL,
  `Role` int(10) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Username` varchar(100) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `Address` text,
  `City` varchar(50) DEFAULT NULL,
  `Province` varchar(50) DEFAULT NULL,
  `Country` varchar(50) DEFAULT NULL,
  `Phone` varchar(100) DEFAULT NULL,
  `BBMAccount` varchar(50) DEFAULT NULL,
  `YMAccount` varchar(100) DEFAULT NULL,
  `FilesName` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table lhbu.user: 5 rows
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `NamaLengkap`, `Position`, `Role`, `Email`, `Username`, `Password`, `Address`, `City`, `Province`, `Country`, `Phone`, `BBMAccount`, `YMAccount`, `FilesName`) VALUES
	(19, 'imam', 'Supervisor', 2, NULL, 'imam', 'eaccb8ea6090a40a98aa28c071810371', '', NULL, 'jakarta', '', '085255456', NULL, NULL, NULL),
	(20, 'administrator', 'admin', 9, NULL, 'admin', '21232f297a57a5a743894a0e4a801fc3', NULL, NULL, 'jakarta', NULL, '0000099', NULL, NULL, NULL),
	(13, 'Mey Handayani Setiawati', 'User', 1, 'mey@shinhan.co.id', 'mey', '15d7000797a33d7af16c9b052f852768', 'Jl. Lingkar Mega Kuningan Kav. E No.1 Lantai 28, Kuningan Timur\r\nJakarta Selatan, Kode Pos 12950\r\nDKI Jakarta, Indonesia', 'Jakarta', 'Jakarta', 'Indonesia', '082110037061', '212121312', 'mey@shinhan.co.id', '14.jpg'),
	(14, 'Deddy noor Inrvansyah', 'Supervisi', 2, 'deddy@shinhan.co.id', 'dedy', '15d7000797a33d7af16c9b052f852768', 'Jalan Deddy Shinhan', 'Jakarta', 'Jakarta', 'Jakarta', '082110037061', '212121312', 'deddy@shinhan.co.id', '14.jpg'),
	(15, 'Feby', 'User', 2, 'feby@shinhan.co.id', 'feby', '3685d30ad28793a74da7bbebddbef08b', 'Jalan Shinhan', 'Jakarta', 'Jakarta', 'Indonesia', '082110037061', '212121312', 'feby@shinhan.co.id', '14.jpg');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
